
//библиотека для запросов
const fetch = require('isomorphic-fetch');

function coolNumbers(num) {
    if (num === 4) 
    { 
    	return false 
    };
    if (num === 1) 
    	{ 
    		return true 
    	};

    //разделяем число по цифрам
    let digArr = ('' + num).split('');
    //сумма квадратов цифер
    let sumOfSquare = digArr.reduce((prev, item) => 
    {
        return ((item * item) + prev)
    }, 0);
    return coolNumbers(sumOfSquare);
}

//если i наше число то пушим его в массив
let coolNumbersArr = [];
for (let i = 1; i <= 1000000; i++) 
{
    if (coolNumbers(i)) 
    {
        coolNumbersArr.push(i);
    }
}
//сумма всех подходящих чисел
let sumOfCoolNumbers = coolNumbersArr.reduce((prev, item) => 
{
    return prev + item
}, 0);


let requests = [];
//массив запросов
for (let i = 0; i <= 100; ++i) 
{
    requests.push(fetch(`http://dev.getethos.com/code${i}`, 
    {
        method: "post",
        headers: 
        {
            "X-COOL-SUM": sumOfCoolNumbers
        }
    }).then((res) => 
    	{
        //если запрос успешен - возвращаем текст
        if(res.status == 200) 
        {
            return res.text()
        }
    }, (err) => 
    {
        console.log(new Error(err))
    }))
}
//благодаря всем успешным запросам и промисам мы получаем наш ключ для входа
Promise.all(requests).then((res) => {
    console.log(`The entrance code is "${res.join('')}"`);
})